<?php


namespace Domain;

use App\Domain\Calculator;
use App\Domain\Exception\CalculatorException;
use Faker\Factory;
use TestCase;

class CalculatorTest extends TestCase
{
    private $faker;

    protected function setUp(): void
    {
        parent::setUp();
        $this->faker = Factory::create();
    }

    public function testItShouldCreateCalculator()
    {
        $payloads = [
            'gender' => 'Pria',
            'weight' => 50,
            'height' => 170,
            'age' => 24,
            'activity' => 'Ringan'
        ];

        $calculator = Calculator::create($payloads);
        $this->assertInstanceOf(Calculator::class, $calculator);
        $this->assertEquals($calculator->getGender(), $payloads['gender']);
        $this->assertEquals($calculator->getWeight(), $payloads['weight']);
        $this->assertEquals($calculator->getHeight(), $payloads['height']);
        $this->assertEquals($calculator->getAge(), $payloads['age']);
        $this->assertEquals($calculator->getActivity(), $payloads['activity']);
    }

    public function testCreateFailedEmptyGender()
    {
        $payloads = [
            'gender' => '',
            'weight' => 50,
            'height' => 170,
            'age' => 24,
            'activity' => 'Ringan'
        ];

        $this->expectException(CalculatorException::class);
        $this->expectExceptionMessage('Gender must not empty!');
        Calculator::create($payloads);
    }

    public function testCreateFailedEmptyWeight()
    {
        $payloads = [
            'gender' => 'Pria',
            'weight' => null,
            'height' => 170,
            'age' => 24,
            'activity' => 'Ringan'
        ];

        $this->expectException(CalculatorException::class);
        $this->expectExceptionMessage('Weight must not empty!');
        Calculator::create($payloads);
    }

    public function testCreateFailedEmptyHeight()
    {
        $payloads = [
            'gender' => 'Pria',
            'weight' => 50,
            'height' => null,
            'age' => 24,
            'activity' => 'Ringan'
        ];

        $this->expectException(CalculatorException::class);
        $this->expectExceptionMessage('Height must not empty!');
        Calculator::create($payloads);
    }

    public function testCreateFailedEmptyAge()
    {
        $payloads = [
            'gender' => 'Pria',
            'weight' => 50,
            'height' => 170,
            'age' => null,
            'activity' => 'Ringan'
        ];

        $this->expectException(CalculatorException::class);
        $this->expectExceptionMessage('Age must not empty!');
        Calculator::create($payloads);
    }

    public function testCreateFailedEmptyActivity()
    {
        $payloads = [
            'gender' => 'Pria',
            'weight' => 50,
            'height' => 170,
            'age' => 24,
            'activity' => ''
        ];

        $this->expectException(CalculatorException::class);
        $this->expectExceptionMessage('Activity must not empty!');
        Calculator::create($payloads);
    }

    public function testItShouldGetBMR()
    {
        $payloads = [
            'gender' => 'Pria',
            'weight' => 50,
            'height' => 170,
            'age' => 24,
            'activity' => 'Ringan'
        ];

        $calculator = Calculator::create($payloads);

        $response = Calculator::getBMR($calculator);
        $this->assertIsNumeric($response);
    }

    public function testGetBmrFailedGenderNotAvailable()
    {
        $payloads = [
            'gender' => 'Laki-laki',
            'weight' => 50,
            'height' => 170,
            'age' => 24,
            'activity' => 'Ringan'
        ];

        $calculator = Calculator::create($payloads);

        $this->expectException(CalculatorException::class);
        $this->expectExceptionMessage('Gender not available!');
        Calculator::getBMR($calculator);
    }
}
