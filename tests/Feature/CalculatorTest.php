<?php


namespace Feature;

use Faker\Factory;
use TestCase;

class CalculatorTest extends TestCase
{
    private $faker;

    protected function setUp(): void
    {
        parent::setUp();
        $this->faker = Factory::create();
    }

    public function testItShouldCalculateMinimumCalorieNeeded()
    {
        $payloads = [
            'gender' => 'Pria',
            'weight' => 50,
            'height' => 170,
            'age' => 24,
            'activity' => 'Ringan'
        ];

        $response = $this->json('POST', route('post.calculate.minimum.calorie.needed'), $payloads);
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            'status' => [
                'code',
                'message'
            ],
            'data'
        ]);
    }
}
