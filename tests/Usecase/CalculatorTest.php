<?php


namespace Usecase;


use App\Domain\Exception\CalculatorException;
use App\Usecase\CalorieCalculateUseCase;
use Faker\Factory;
use TestCase;

class CalculatorTest extends TestCase
{
    private $faker;

    protected function setUp(): void
    {
        parent::setUp();
        $this->faker = Factory::create();
    }

    public function testItShouldCalculateMinimumCalorie()
    {
        $payloads = [
            'gender' => 'Pria',
            'weight' => 50,
            'height' => 170,
            'age' => 24,
            'activity' => 'Ringan'
        ];

        $response = (new CalorieCalculateUseCase())->execute($payloads);
        $this->assertIsNumeric($response);
    }

    public function testItShouldCalculateMinimumCalorieActivityRingan()
    {
        $payloads = [
            'gender' => 'Pria',
            'weight' => 50,
            'height' => 170,
            'age' => 24,
            'activity' => 'Ringan'
        ];

        $response = (new CalorieCalculateUseCase())->execute($payloads);
        $this->assertIsNumeric($response);
    }

    public function testItShouldCalculateMinimumCalorieActivitySedang()
    {
        $payloads = [
            'gender' => 'Wanita',
            'weight' => 50,
            'height' => 170,
            'age' => 24,
            'activity' => 'Sedang'
        ];

        $response = (new CalorieCalculateUseCase())->execute($payloads);
        $this->assertIsNumeric($response);
    }

    public function testItShouldCalculateMinimumCalorieActivityBerat()
    {
        $payloads = [
            'gender' => 'Pria',
            'weight' => 50,
            'height' => 170,
            'age' => 24,
            'activity' => 'Berat'
        ];

        $response = (new CalorieCalculateUseCase())->execute($payloads);
        $this->assertIsNumeric($response);
    }

    public function testCalculateMinimumCalorieFailedEmptyGender()
    {
        $payloads = [
            'gender' => '',
            'weight' => 50,
            'height' => 170,
            'age' => 24,
            'activity' => 'Ringan'
        ];

        $this->expectException(CalculatorException::class);
        $this->expectExceptionMessage('Gender must not empty!');
        (new CalorieCalculateUseCase())->execute($payloads);
    }

    public function testCalculateMinimumCalorieFailedNotAvailableActivity()
    {
        $payloads = [
            'gender' => 'Pria',
            'weight' => 50,
            'height' => 170,
            'age' => 24,
            'activity' => 'Biasa aja'
        ];

        $this->expectException(CalculatorException::class);
        $this->expectExceptionMessage('Activity not available');
        (new CalorieCalculateUseCase())->execute($payloads);
    }
}
