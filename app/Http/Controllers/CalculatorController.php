<?php


namespace App\Http\Controllers;


use App\Usecase\CalorieCalculateUseCase;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CalculatorController
{
    public function calculateMinimumCalorie(Request $request, CalorieCalculateUseCase $useCase): JsonResponse
    {
        $calculate = $useCase->execute($request->all());

        return response()->json([
            'status' => [
                'code' => 200,
                'message' => 'Success'
            ],
            'data' => $calculate
        ]);
    }
}
