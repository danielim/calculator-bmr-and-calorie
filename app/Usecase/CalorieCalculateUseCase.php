<?php


namespace App\Usecase;


use App\Domain\Calculator;
use App\Domain\Exception\CalculatorException;

class CalorieCalculateUseCase
{
    public function execute(array $payloads)
    {
        $calculator = Calculator::create($payloads);
        $bmr = Calculator::getBMR($calculator);

        return match ($calculator->getActivity()) {
            'Ringan' => $bmr * 1.2,
            'Sedang' => $bmr * 1.3,
            'Berat' => $bmr * 1.4,
            default => throw new CalculatorException('Activity not available')
        };
    }
}
