<?php


namespace App\Domain;


use App\Domain\Exception\CalculatorException;

class Calculator
{
    public function __construct(
        private string $gender,
        private int $weight,
        private int $height,
        private int $age,
        private string $activity,
    )
    {}

    /**
     * @return string
     */
    public function getGender(): string
    {
        return $this->gender;
    }

    /**
     * @return int
     */
    public function getWeight(): int
    {
        return $this->weight;
    }

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * @return string
     */
    public function getActivity(): string
    {
        return $this->activity;
    }

    public static function create(array $payloads)
    {
        $gender = isset($payloads['gender']) ? $payloads['gender'] : null;
        if (empty($gender)) {
            throw new CalculatorException('Gender must not empty!');
        }
        $weight = isset($payloads['weight']) ? $payloads['weight'] : null;
        if (empty($weight)) {
            throw new CalculatorException('Weight must not empty!');
        }
        $height = isset($payloads['height']) ? $payloads['height'] : null;
        if (empty($height)) {
            throw new CalculatorException('Height must not empty!');
        }
        $age = isset($payloads['age']) ? $payloads['age'] : null;
        if (empty($age)) {
            throw new CalculatorException('Age must not empty!');
        }
        $activity = isset($payloads['activity']) ? $payloads['activity'] : null;
        if (empty($activity)) {
            throw new CalculatorException('Activity must not empty!');
        }

        return new self($gender, $weight, $height, $age, $activity);
    }

    public static function getBMR(Calculator $calculator)
    {
        match ($calculator->getGender()) {
            'Pria' => [
                $weight = $calculator->getWeight() * 13.7,
                $height = $calculator->getHeight() * 5,
                $age = $calculator->getAge() * 6.8,
                $bmr = 66.5 + $weight + $height - $age,
            ],
            'Wanita' => [
                $weight = $calculator->getWeight() * 9.6,
                $height = $calculator->getHeight() * 1.8,
                $age = $calculator->getAge() * 4.7,
                $bmr = 655 + $weight + $height - $age,
            ],
            default => throw new CalculatorException('Gender not available!')
        };

        return $bmr;
    }
}
